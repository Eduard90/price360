import uuid
import os
import re
from datetime import datetime
from collections import OrderedDict
from django.utils import six
from django.shortcuts import render, redirect
from django.views.generic import ListView, CreateView, UpdateView
from django import forms, urls
from django.core.exceptions import ValidationError
from django.db.models import Q, expressions, CharField, IntegerField, F
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.messages.views import SuccessMessageMixin
from django.conf import settings
import django_filters
from django_filters.filterset import BaseFilterSet, FilterSetMetaclass
import django_excel as excel
import pyexcel as pe

from price360.models import Category, Product, Field, Shop, FilterFieldValue, ImportResult
from currency.models import Currency, CurrencyRate
from price360.forms import ImportStep1Form, ImportStep2Form, ImportStep3Form


def index(request):
    shop_categories = Category.objects.filter(shop=request.user.last_shop).all()
    products_cnt = Product.objects.filter(category__in=shop_categories).count()
    context = {
        'products_cnt': products_cnt
    }
    return render(request, 'price360/index.html', context=context)


def change_shop(request, shop_id: int = None):
    try:
        shop = Shop.objects.get(pk=shop_id)
    except Shop.DoesNotExist:
        shop = None

    request.user.last_shop = shop
    request.user.save()
    return redirect('/')


class CustomFilterSetMetaclass(FilterSetMetaclass):
    def __call__(cls, *args, **kwargs):
        json_fields = []
        for key, field in cls.base_filters.items():
            if key.startswith('json_'):
                json_fields.append(key)

        for json_field in json_fields:
            del cls.base_filters[json_field]
        category_obj = kwargs.pop('category_obj', None)
        if category_obj:
            category_fields = Field.objects.active().in_filter().filter(
                category__in=category_obj.get_ancestors(include_self=True)).all()
            for field in category_fields:
                try:
                    json_field_filter = FilterFieldValue.objects.get(field=field)
                    values = (
                        (val, "{prefix}{val}{postfix}".format(prefix=field.prefix, val=val, postfix=field.postfix))
                        for val in json_field_filter.values)
                    cls.base_filters['json_{}'.format(field.code)] = django_filters.ChoiceFilter(label=field.name,
                                                                                                 choices=values,
                                                                                                 method='filter_json_field',
                                                                                                 empty_label='Все',
                                                                                                 name=field.code)
                except FilterFieldValue.DoesNotExist:
                    pass

        return super().__call__(*args, **kwargs)


class CustomBaseFilterSet(BaseFilterSet):
    pass


class FilterSet(six.with_metaclass(CustomFilterSetMetaclass, CustomBaseFilterSet)):
    pass


class ProductFilter(FilterSet):
    IN_STOCK_ALL = 'all'
    IN_STOCK_YES = 'in_stock'
    IN_STOCK_NO = 'no'
    IN_STOCK_LIST = (
        (IN_STOCK_YES, 'В наличии'),
        (IN_STOCK_NO, 'Нет'),
    )

    code = django_filters.CharFilter(lookup_expr='icontains', label='Код',
                                     widget=forms.TextInput(attrs={'placeholder': 'Код...'}))
    name = django_filters.CharFilter(lookup_expr='icontains', label='Название',
                                     widget=forms.TextInput(attrs={'placeholder': 'Название...'}))
    in_stock = django_filters.ChoiceFilter(label='Наличие', choices=IN_STOCK_LIST, method='filter_in_stock',
                                           empty_label='Все')

    def __init__(self, data=None, queryset=None, prefix=None, strict=None, request=None, category_obj=None):
        super().__init__(data=data, queryset=queryset, prefix=prefix, strict=strict, request=request)

    def filter_json_field(self, queryset, name, value):
        try:
            field = Field.objects.get(code=name)
            converter = Field.TYPES_CONVERT[field.field_type]
            cond = {'fields__{}'.format(name): converter.convert(value)}
            qs = queryset.filter(**cond)
        except Field.DoesNotExist:
            qs = queryset
        return qs

    def filter_in_stock(self, queryset, name, value):
        # TODO: Need check this conditions
        if value == self.IN_STOCK_YES:
            queryset = queryset.filter(Q(stock_num__isempty=False) | Q(stock=Product.STOCK_YES))
        elif value == self.IN_STOCK_NO:
            queryset = queryset.filter(Q(stock_num__isempty=True) | (Q(stock=Product.STOCK_NO)))  # FIXME: Really? Check

        return queryset

    class Meta:
        model = Product
        fields = ['code', 'name', 'in_stock']


def category(request, category_id: int = None):
    category_obj = Category.objects.get(pk=category_id)
    products = Product.objects
    if category_obj.is_leaf_node():
        filter_ = {'category': category_obj}
    else:
        filter_ = {'category__in': category_obj.get_descendants(True)}

    products = products.filter(**filter_).select_related('manufacturer', 'wholesale_currency', 'retail_currency')

    wholesale_currency_display = request.GET.get('wholesale_currency_display', None)
    if wholesale_currency_display and len(wholesale_currency_display):
        wholesale_currency_display = wholesale_currency_display.upper()
        try:
            wholesale_currency = Currency.objects.get(code=wholesale_currency_display)
        except Currency.DoesNotExist:
            wholesale_currency_display = None

    retail_currency_display = request.GET.get('retail_currency_display', None)
    if retail_currency_display and len(retail_currency_display):
        retail_currency_display = retail_currency_display.upper()
        try:
            retail_currency = Currency.objects.get(code=retail_currency_display)
        except Currency.DoesNotExist:
            retail_currency_display = None

    f = ProductFilter(request.GET, queryset=products, category_obj=category_obj)
    category_fields = Field.objects.active().filter(category__in=category_obj.get_ancestors(include_self=True)).all()

    filter_query_fields = {}
    if f.form.is_valid():  # Need for get cleaned_data
        filter_query_fields = f.form.cleaned_data

    paginator = Paginator(f.qs, 50)
    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    currency_cache = {}
    currency_rate_cache = {}

    context = {'category': category_obj, 'products': products, 'category_fields': category_fields, 'filter': f,
               'wholesale_currency_display': wholesale_currency_display,
               'retail_currency_display': retail_currency_display, 'page': paginator,
               'filter_query_fields': filter_query_fields,
               'currency_rate_cache': currency_rate_cache, 'currency_cache': currency_cache}
    return render(request, 'price360/product/category.html', context)


def move_products(request):
    if request.method == 'POST':
        products_ids = request.POST.getlist('product_selected')
        category_id = request.POST.get('category_id', None)
        if products_ids and category_id is not None:
            # TODO: Need check shop
            Product.objects.filter(pk__in=products_ids).update(category_id=category_id)

    return redirect(request.META.get('HTTP_REFERER'))


def export(request, category_id: int = None):
    products = []
    category_obj = Category.objects.get(pk=category_id)  # type: Category
    if category_obj.is_leaf_node():
        filter_ = {'category': category_obj}
    else:
        filter_ = {'category__in': category_obj.get_descendants(True)}
    query_sets = Product.objects.filter(**filter_).select_related('manufacturer', 'wholesale_currency',
                                                                  'retail_currency').order_by('id').all()
    category_fields = Field.objects.active().filter(category__in=category_obj.get_ancestors(include_self=True)).all()
    # category_fields = Field.objects.filter(**filter_).order_by('sort').all()
    category_fields_dict = OrderedDict()
    for field in category_fields:
        category_fields_dict[field.code] = field.name
    for product_qs in query_sets.iterator():
        product = OrderedDict()
        product['#'] = product_qs.id
        product['Код'] = product_qs.code
        product['Название'] = product_qs.name
        product['Производитель'] = product_qs.manufacturer.name
        product['Гарантия (мес.)'] = product_qs.guarantee
        product['Оптовая цена'] = "{}{}".format(product_qs.wholesale_price, product_qs.wholesale_currency.symbol)
        product['Розничная цена'] = "{}{}".format(product_qs.retail_price, product_qs.retail_currency.symbol)
        for code, name in category_fields_dict.items():
            if type(product_qs.fields) == dict:
                try:
                    field_value = product_qs.fields[code]
                    product[category_fields_dict[code]] = field_value
                except KeyError:
                    pass

        products.append(product)

    return excel.make_response_from_records(
        products,
        'xls',
        file_name="{} {}".format(category_obj.name, datetime.now().strftime("%d_%m_%Y %H:%M:%S")),
    )


class CategoryListView(ListView):
    model = Category
    template_name = 'price360/category/list.html'


class CategoryCreateView(SuccessMessageMixin, CreateView):
    model = Category
    template_name = 'price360/category/create.html'
    success_url = urls.reverse_lazy('settings-category-list')
    fields = '__all__'
    success_message = 'Категория "%(name)s" создана'


class CategoryUpdateView(SuccessMessageMixin, UpdateView):
    model = Category
    template_name = 'price360/category/update.html'
    success_url = urls.reverse_lazy('settings-category-list')
    fields = '__all__'
    success_message = 'Категория "%(name)s" изменена'
    context_object_name = 'category_obj'


def import_step_1(request):
    if 'import_import_result' in request.session:
        del request.session['import_import_result']

    if request.method.lower() == 'post':
        form = ImportStep1Form(request.POST, request.FILES)
        if form.is_valid():
            filename = str(uuid.uuid4())
            path = "{}/{}.xls".format(settings.IMPORT_DIR, filename)
            with open(path, "w+b") as f:
                f.write(form.cleaned_data['file'].read())
                request.session['import_filename'] = "{}.xls".format(filename)
                return redirect('/products/import/step/2/')

            print("VALID!")
    else:
        form = ImportStep1Form()
        request.session['import'] = {}

    context = {'form': form}

    return render(request, 'price360/import/step1.html', context=context)


def import_step_2(request):
    # context = {'categories': Category.objects.filter(shop=request.user.last_shop).all()}
    context = {}
    filename = request.session['import_filename']
    full_path = os.path.join(settings.IMPORT_DIR, filename)
    if os.path.exists(full_path):
        book = pe.get_book(file_name=full_path)
        sheets = ((sheet, sheet) for sheet in book.sheet_names())
        # context['sheets'] = book.sheet_names()

        if request.method.lower() == 'post':
            # sheet = request.POST.get('sheet', None)
            form = ImportStep2Form(request.POST, sheets=sheets, user=request.user)
            if form.is_valid():
                if form.cleaned_data['sheet'] in book.sheet_names():
                    request.session['import_sheet'] = form.cleaned_data['sheet']
                    request.session['import_category'] = form.cleaned_data['category'].pk
                    return redirect('/products/import/step/3/')

        else:
            form = ImportStep2Form(sheets=sheets, user=request.user)
            context['form'] = form

    return render(request, 'price360/import/step2.html', context=context)


def import_step_3(request):
    stock_regexp = re.compile("\d+")

    def parse_stock_raw(stock_raw: str):
        num = stock_regexp.findall(stock_raw)
        if len(num) == 2:  # Example: [10, 20]
            return [int(num[0]), int(num[1])]
        elif len(num) == 1:  # Example: [10]
            return [1, int(num[0])]
        else:
            return None

    # print(request.session['import_filename'])
    # print(request.session['import_sheet'])
    category_obj = Category.objects.get(pk=request.session['import_category'])
    category_fields = Field.objects.active().filter(category__in=category_obj.get_ancestors(include_self=True)).all()

    required_fields = ['code', 'name', 'wholesale_price', 'wholesale_currency', 'retail_price', 'retail_currency',
                       'stock_raw', 'manufacturer', 'guarantee']

    filename = request.session['import_filename']
    full_path = os.path.join(settings.IMPORT_DIR, filename)

    if request.method.lower() == 'post':
        form = ImportStep3Form(request.POST)
        if form.is_valid():
            # TODO: Need modify for different imports
            retail_currency = form.cleaned_data['retail_currency']
            wholesale_currency = form.cleaned_data['wholesale_currency']
            manufacturer = form.cleaned_data['manufacturer']
            guarantee = form.cleaned_data['guarantee']

            system_fields = {}  # TODO: Maybe rename to fields? It's ALL fields
            for key, val in request.POST.items():
                if 'col-' in key:
                    system_fields[val] = key

            # print(system_fields)
            system_fields['retail_currency'] = retail_currency
            system_fields['wholesale_currency'] = wholesale_currency
            system_fields['manufacturer'] = manufacturer
            system_fields['guarantee'] = guarantee

            import_result = ImportResult(filename=request.session['import_filename'], user=request.user,
                                         shop=request.user.last_shop, rows=0, success_rows=0)
            import_result.save()
            rows = 0
            success_rows = 0
            book = pe.get_book(file_name=full_path)
            sheet = book[request.session['import_sheet']]
            for row in sheet:
                stock = Product.STOCK_YES
                stock_num = [0, 0]
                rows += 1

                product = Product()
                for required_field in required_fields:
                    field_value = system_fields[required_field]
                    if type(field_value) == str and 'col-' in field_value:
                        field_value = row[int(field_value.replace('col-', ''))]
                        if 'stock_raw' in required_field:

                            if 'Нет' in field_value:  # TODO: Need many different - "Нет", "Да", "Есть", "Остаток".
                                stock = Product.STOCK_NO

                            if stock == Product.STOCK_YES:  # TODO: Need parse range in 'stock_raw' field
                                stock_num_parsed = parse_stock_raw(field_value)
                                if stock_num_parsed is not None:
                                    stock_num = stock_num_parsed
                                else:
                                    stock_num = [1, 10]

                    # try:
                    setattr(product, required_field, field_value)

                json_fields = {}
                for category_field in category_fields:
                    if category_field.code in system_fields:
                        col = int(system_fields[category_field.code].replace('col-', ''))
                        value_raw = row[col]
                        try:
                            value = category_field.get_converter().extract(value_raw)
                            json_fields[category_field.code] = value
                        except RuntimeError:
                            pass

                product.import_result = import_result
                product.category = category_obj
                product.stock = stock
                product.stock_num = stock_num
                product.fields = json_fields

                if not product.code and not product.name:
                    continue

                try:
                    product_db = Product.objects.get(code=product.code, category=category_obj)
                    product.pk = product_db.pk  # For update
                except Product.DoesNotExist:
                    pass

                try:
                    product.save()
                    success_rows += 1
                except ValidationError:
                    continue

            import_result.rows = rows
            import_result.success_rows = success_rows
            import_result.save(update_fields=['rows', 'success_rows'])
            request.session['import_import_result'] = import_result.pk
            return redirect('/products/import/step/4/')

    preview_rows = []
    if os.path.exists(full_path):
        book = pe.get_book(file_name=full_path)
        sheet = book[request.session['import_sheet']]
        max_rows = 8
        cur_row = 0
        for row in sheet:
            if cur_row >= max_rows:
                break

            preview_rows.append(row)
            cur_row += 1

    form = ImportStep3Form()

    context = {'preview_rows': preview_rows, 'required_fields': required_fields, 'form': form,
               'category_fields': category_fields}
    return render(request, 'price360/import/step3.html', context=context)


def import_step_4(request):
    context = {}
    if 'import_import_result' in request.session:
        try:
            import_result = ImportResult.objects.get(pk=request.session['import_import_result'])
            context['import_result'] = import_result

            import_session_keys = ['import_filename', 'import_sheet', 'import_category']
            for key in import_session_keys:
                if key in request.session:
                    del request.session[key]

            return render(request, 'price360/import/step4.html', context=context)
        except ImportResult.DoesNotExist:
            redirect('/products/import/')
