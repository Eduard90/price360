from price360.models import Shop


def shopMiddleware(get_response):
    def middleware(request):
        if request.user.is_authenticated:
            last_shop = request.user.last_shop
            if last_shop is None:
                first_shop = Shop.objects.first()
                request.user.last_shop = first_shop
                request.user.save()

        response = get_response(request)
        return response

    return middleware
