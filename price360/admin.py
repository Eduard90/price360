import json
from django.contrib import admin
from django import forms
from django.utils.safestring import mark_safe
from django.template.loader import render_to_string
from mptt.admin import DraggableMPTTAdmin
# Register your models here.

from price360.models import ImportResult, Category, Product, Field, Shop, Manufacturer, FilterFieldValue


class ProductFieldsWidget(forms.Widget):
    template_name = "widgets/product_fields_widget.html"
    fields = {}

    def __init__(self, attrs=None):
        super().__init__(attrs)

    def render(self, name, value, attrs=None, renderer=None):
        self.fields = {}
        all_fields = Field.objects.filter(is_active=True).all()
        category_ids = []
        for field in all_fields:
            if field.category.pk not in self.fields:
                category_ids.append(field.category.pk)
                self.fields[field.category.pk] = []
            self.fields[field.category.pk].append({'code': field.code, 'name': field.name})

        for category_id in category_ids:
            category = Category.objects.get(pk=category_id)
            if category.parent is not None:
                parent_fields = self.fields[category.parent.pk]
                self.fields[category.pk] += parent_fields

        context = {
            'name': name,
            'value': value,
            'fields': json.dumps(self.fields),
        }
        return mark_safe(render_to_string(self.template_name, context))


class ProductAdminForm(forms.ModelForm):
    class Meta:
        model = Product
        widgets = {
            'fields': ProductFieldsWidget(),
        }
        fields = '__all__'


@admin.register(Shop)
class ShopModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name')
    list_display_links = ('id', 'code', 'name')


@admin.register(ImportResult)
class ImportResultModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'filename', 'rows', 'success_rows', 'user')
    list_display_links = ('id', 'filename')


@admin.register(Category)
class CategoryModelAdmin(DraggableMPTTAdmin):
    list_display = ('tree_actions', 'indented_title', 'products_cnt', 'fields_')

    def fields_(self, obj: Category):
        fields = ["{} ({})".format(field.name, field.code) for field in Field.objects.filter(category=obj).all()]
        return ', '.join(fields)

    def products_cnt(self, obj: Category):
        return obj.product_set.count()

    fields_.short_description = "Поля"
    products_cnt.short_description = "Кол-во товаров"


@admin.register(Product)
class ProductModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'category', 'code', 'name', 'json_fields')
    list_display_links = ('id', 'code', 'name')
    list_filter = ('category',)
    form = ProductAdminForm
    all_json_fields = {}

    def get_changelist(self, request, **kwargs):
        all_fields = Field.objects.all()
        self.all_json_fields = {field.code: field for field in all_fields}
        return super().get_changelist(request, **kwargs)

    def json_fields(self, obj: Product):
        fields_formatted = []
        if type(obj.fields) == dict:
            for key, val in obj.fields.items():
                try:
                    field_formatted = "<b>{}</b>: {}".format(self.all_json_fields[key], val)
                    fields_formatted.append(field_formatted)
                except KeyError:
                    pass

        return '<br>'.join(fields_formatted)

    json_fields.allow_tags = True
    json_fields.short_description = "Поля"


@admin.register(Field)
class FieldModelAdmin(admin.ModelAdmin):
    list_filter = ('category',)
    list_display = ('id', 'code', 'name', 'category', 'is_active', 'sort')
    list_display_links = ('id', 'code', 'name')
    list_editable = ('sort',)


@admin.register(Manufacturer)
class ManufacturerModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')


@admin.register(FilterFieldValue)
class FielterFieldValueModelAdmin(admin.ModelAdmin):
    list_display = ('field', 'values')
