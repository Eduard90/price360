from django.conf.urls import url

from price360.views import index, category, change_shop, move_products, export, CategoryListView, import_step_1, \
    import_step_2, import_step_3, import_step_4, CategoryCreateView, CategoryUpdateView

from price360.import_views.evrosvet import evrosvet_import_step_1, evrosvet_import_step_2

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^change_shop/(?P<shop_id>[0-9])/$', change_shop, name='change-shop'),

    url(r'^category/$', CategoryListView.as_view(), name='settings-category-list'),
    url(r'^category/create/$', CategoryCreateView.as_view(), name='settings-category-create'),
    url(r'^category/(?P<pk>[0-9])/$', CategoryUpdateView.as_view(), name='settings-category-update'),

    url(r'^products/category/(?P<category_id>[0-9])/$', category, name='category'),
    url(r'^products/move/$', move_products, name='move-products'),
    url(r'^products/export/(?P<category_id>[0-9])/$', export, name='export'),

    url(r'^products/import/$', import_step_1, name='import-step-1'),
    url(r'^products/import/step/2/$', import_step_2, name='import-step-2'),
    url(r'^products/import/step/3/$', import_step_3, name='import-step-3'),
    url(r'^products/import/step/4/$', import_step_4, name='import-step-4'),

    url(r'^products/import_evrosvet/$', evrosvet_import_step_1, name='import-evrosvet-step-1'),
    url(r'^products/import_evrosvet/step/2/$', evrosvet_import_step_2, name='import-evrosvet-step-2'),

]
