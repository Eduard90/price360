from django import forms
from mptt.forms import TreeNodeChoiceField

from price360.models import Category, Currency, Manufacturer


class ImportStep1Form(forms.Form):
    file = forms.FileField(label="Excel файл")


class ImportStep2Form(forms.Form):
    sheet = forms.ChoiceField(label="Страница")
    category = TreeNodeChoiceField(queryset=Category.objects.all(), label="Категория")

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)  # TODO: Need check if None
        sheets = kwargs.pop('sheets', [])
        super().__init__(*args, **kwargs)
        # self.fields['category'].queryset = Category.objects.filter(shop=user.last_shop).all()
        self.fields['sheet'].choices = sheets
        self.fields['category'].queryset = Category.objects.filter(shop=user.last_shop).all()


class ImportStep3Form(forms.Form):
    wholesale_currency = forms.ModelChoiceField(queryset=Currency.objects.all(), label="Опт. валюта")
    retail_currency = forms.ModelChoiceField(queryset=Currency.objects.all(), label="Розн. валюта")
    manufacturer = forms.ModelChoiceField(queryset=Manufacturer.objects.all(), label="Производитель")
    guarantee = forms.CharField(label="Гарантия (мес.)")
