from django.core.management.base import BaseCommand, CommandError

from price360.models import Field, Product, Category, FilterFieldValue


class Command(BaseCommand):
    help = 'Run update field values for filter'

    def handle(self, *args, **options):
        fields_vals = {}
        products = Product.objects.all()
        for product in products:
            products_fields = product.fields
            if products_fields is not None and type(products_fields) == dict:
                for field, val in products_fields.items():
                    if field not in fields_vals:
                        fields_vals[field] = []

                    if val not in fields_vals[field]:
                        fields_vals[field].append(val)

        for field, val in fields_vals.items():
            try:
                field_obj = Field.objects.get(code=field, is_active=True)
            except Field.DoesNotExist:
                self.stdout.write(self.style.ERROR("Can't find field '{}'".format(field)))
                continue
            val.sort()  # TODO: Sort values list. Maybe need custom sorting
            filter_val = FilterFieldValue.objects.update_or_create(field=field_obj, defaults={'values': val})
