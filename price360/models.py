from os import uname_result

from django.db import models
from django.contrib.postgres.fields import JSONField, IntegerRangeField
from mptt.models import MPTTModel, TreeForeignKey, TreeManager
from model_utils.models import TimeStampedModel
from dirtyfields import DirtyFieldsMixin

from currency.models import Currency
from price360.filter_field import TypeBool, TypeFloat, TypeInt, TypeString


class FieldQuerySet(models.QuerySet):
    def active(self):
        return self.filter(is_active=True)

    def in_filter(self):
        return self.filter(show_in_filter=True)


class Shop(DirtyFieldsMixin, TimeStampedModel, models.Model):
    code = models.CharField("Код", max_length=50, blank=False)
    name = models.CharField("Название", max_length=200, blank=False)
    is_active = models.BooleanField("Активность", default=True)

    class Meta:
        verbose_name = "Магазин"
        verbose_name_plural = "Магазины"

    def __str__(self):
        return "{} ({})".format(self.name, self.code)


class ImportResult(DirtyFieldsMixin, TimeStampedModel, models.Model):
    IMPORT_EVROSVET = "evrosvet"

    IMPORT_LIST = (
        (IMPORT_EVROSVET, 'Evrosvet'),
    )

    filename = models.CharField("Имя файла", max_length=200, blank=False)
    rows = models.PositiveIntegerField("Кол-во товаров", blank=False)
    success_rows = models.PositiveIntegerField("Кол-во обработанных товаров", blank=False)
    user = models.ForeignKey("accounts.User", verbose_name="Пользователь", blank=False)
    shop = models.ForeignKey(Shop, verbose_name="Магазин")
    import_price = models.CharField("Название", choices=IMPORT_LIST, null=True, max_length=50)

    class Meta:
        verbose_name = "Импорт"
        verbose_name_plural = "Импорты"

    def __str__(self):
        return "{} в {} ({})".format(self.filename, self.created, self.user)


class Manufacturer(DirtyFieldsMixin, TimeStampedModel, models.Model):
    name = models.CharField("Название", max_length=200, blank=False)

    class Meta:
        verbose_name = "Производитель"
        verbose_name_plural = "Производители"

    def __str__(self):
        return self.name


class Category(DirtyFieldsMixin, TimeStampedModel, MPTTModel):
    name = models.CharField("Название", max_length=250)
    parent = TreeForeignKey('self', verbose_name='Родительская категория', null=True, blank=True,
                            related_name='children', db_index=True)
    shop = models.ForeignKey(Shop, verbose_name="Магазин")

    # objects = TreeManager()

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"

    def __str__(self):
        return "{} ({})".format(self.name, self.shop.code)


class Field(DirtyFieldsMixin, models.Model):
    TYPE_STR = 'string'
    TYPE_INT = 'int'
    TYPE_FLOAT = 'float'
    TYPE_BOOL = 'bool'
    TYPES_LIST = (
        (TYPE_STR, 'String'),
        (TYPE_INT, 'Integer'),
        (TYPE_FLOAT, 'Float'),
        (TYPE_BOOL, 'Boolean'),
    )
    TYPES_CONVERT = {
        TYPE_STR: TypeString,
        TYPE_INT: TypeInt,
        TYPE_FLOAT: TypeFloat,
        TYPE_BOOL: TypeBool,
    }

    code = models.CharField("Код поля", max_length=100, blank=False, unique=True)
    name = models.CharField("Название", max_length=100, blank=False)
    category = TreeForeignKey(Category, verbose_name="Категория", related_name='fields')
    field_type = models.CharField("Тип", max_length=50, choices=TYPES_LIST, null=False, blank=False)
    many = models.BooleanField("Множественное", default=False)
    required = models.BooleanField("Обязательное", default=False)
    show_in_filter = models.BooleanField("Показывать в фильтре", default=True)
    prefix = models.CharField("Префикс", max_length=50, blank=True)
    postfix = models.CharField("Постфикс", max_length=50, blank=True)
    is_active = models.BooleanField("Активность", default=True)
    sort = models.PositiveIntegerField("Сортировка", default=500)

    objects = FieldQuerySet.as_manager()

    class Meta:
        ordering = ['sort']
        verbose_name = "Поле"
        verbose_name_plural = "Поля"

    def __str__(self):
        return "{} ({})".format(self.name, self.code)

    def get_converter(self):
        return Field.TYPES_CONVERT[self.field_type]


class FilterFieldValue(TimeStampedModel, models.Model):
    field = models.OneToOneField(Field, verbose_name="Поле", unique=True)
    values = JSONField("Значения", default=[])

    class Meta:
        verbose_name = "Значение поля (фильтр)"
        verbose_name_plural = "Значения поля (фильтр)"

    def __str__(self):
        return "{}".format(self.field)


class Product(DirtyFieldsMixin, TimeStampedModel, models.Model):
    STOCK_YES = 'yes'
    STOCK_NO = 'no'
    STOCK_LIST = (
        (STOCK_YES, 'Да'),
        (STOCK_NO, 'Нет'),
    )
    code = models.CharField("Код", max_length=50)
    name = models.CharField("Название", max_length=250)
    category = TreeForeignKey(Category, verbose_name="Категория")
    manufacturer = models.ForeignKey(Manufacturer, verbose_name="Производитель")
    wholesale_price = models.DecimalField("Опт. цена", max_digits=8, decimal_places=2)
    wholesale_currency = models.ForeignKey(Currency, verbose_name='Валюта (опт.)',
                                           related_name='product_wholesale_currency')
    retail_price = models.DecimalField("Розн. цена", max_digits=8, decimal_places=2)
    retail_currency = models.ForeignKey(Currency, verbose_name='Валюта (розн.)', related_name='product_retail_currency')
    stock_raw = models.CharField("Наличие (RAW)", max_length=100, blank=True, editable=False)
    stock = models.CharField("Наличие", max_length=10, choices=STOCK_LIST, db_index=True)
    stock_num = IntegerRangeField("Наличие (кол-во)", blank=True)
    guarantee = models.PositiveSmallIntegerField("Гарантия (мес.)", blank=True)
    info = models.TextField("Доп. информация", blank=True)
    prices = JSONField(verbose_name="Доп. цены", blank=True, null=True)
    fields = JSONField(verbose_name="Доп. поля", blank=True, null=True)
    import_result = models.ForeignKey(ImportResult, verbose_name="Импорт")

    class Meta:
        verbose_name = "Товар"
        verbose_name_plural = "Товары"

    def __str__(self):
        return "#{} {}".format(self.code, self.name)


class ImportCategory(DirtyFieldsMixin, TimeStampedModel, models.Model):
    import_result = models.ForeignKey(ImportResult, verbose_name="Импорт")
    name = models.CharField("Название", max_length=250)
    shop = models.ForeignKey(Shop, verbose_name="Магазин")

    class Meta:
        verbose_name = "Импорт категория"
        verbose_name_plural = "Импорт категории"

    def __str__(self):
        return "{} ({})".format(self.name, self.shop.code)


class ImportManufacturer(DirtyFieldsMixin, TimeStampedModel, models.Model):
    name = models.CharField("Название", max_length=200, blank=False)

    class Meta:
        verbose_name = "Импорт производитель"
        verbose_name_plural = "Импорт производители"

    def __str__(self):
        return self.name


class ImportProduct(DirtyFieldsMixin, TimeStampedModel, models.Model):
    STOCK_YES = 'yes'
    STOCK_NO = 'no'
    STOCK_LIST = (
        (STOCK_YES, 'Да'),
        (STOCK_NO, 'Нет'),
    )
    code = models.CharField("Код", max_length=50)
    name = models.CharField("Название", max_length=250)
    category = TreeForeignKey(ImportCategory, verbose_name="Импорт категория")
    manufacturer = models.ForeignKey(ImportManufacturer, verbose_name="Производитель")
    wholesale_price = models.DecimalField("Опт. цена", max_digits=8, decimal_places=2)
    wholesale_currency = models.ForeignKey(Currency, verbose_name='Валюта (опт.)',
                                           related_name='import_product_wholesale_currency')
    retail_price = models.DecimalField("Розн. цена", max_digits=8, decimal_places=2)
    retail_currency = models.ForeignKey(Currency, verbose_name='Валюта (розн.)',
                                        related_name='import_product_retail_currency')
    stock_raw = models.CharField("Наличие (RAW)", max_length=100, blank=True, editable=False)
    stock = models.CharField("Наличие", max_length=10, choices=STOCK_LIST, db_index=True)
    stock_num = IntegerRangeField("Наличие (кол-во)", blank=True)
    guarantee = models.PositiveSmallIntegerField("Гарантия (мес.)", blank=True)
    info = models.TextField("Доп. информация", blank=True)
    prices = JSONField(verbose_name="Доп. цены", blank=True, null=True)
    fields = JSONField(verbose_name="Доп. поля", blank=True, null=True)
    import_result = models.ForeignKey(ImportResult, verbose_name="Импорт")

    class Meta:
        verbose_name = "Импорт товар"
        verbose_name_plural = "Импорт товары"

    def __str__(self):
        return "#{} {}".format(self.code, self.name)
