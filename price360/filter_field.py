import re


class Type:
    @staticmethod
    def convert(val):
        raise NotImplemented()

    @staticmethod
    def extract(string: str):
        """
        Extract value from string
        :param string:
        :return:
        """
        raise NotImplemented()


class TypeString(Type):
    @staticmethod
    def convert(val):
        return str(val)

    @staticmethod
    def extract(string: str):
        return string


class TypeInt(Type):
    @staticmethod
    def convert(val):
        return int(val)

    @staticmethod
    def extract(string: str):
        try:
            value = re.findall(r'\d+', string)[0]
            return TypeInt.convert(value)
        except:
            raise RuntimeError("Can't extract and convert TypeInt from '{}'".format(string))


class TypeFloat(Type):
    @staticmethod
    def convert(val):
        return float(val)

    @staticmethod
    def extract(string: str):
        try:
            value = re.findall(r'\d+', string)[0]
            return TypeFloat.convert(value)
        except:
            raise RuntimeError("Can't extract and convert TypeFloat from '{}'".format(string))


class TypeBool(Type):
    @staticmethod
    def convert(val):
        return bool(val)

    @staticmethod
    def extract(string: str):
        return TypeBool.convert(string)
