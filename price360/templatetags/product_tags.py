from django import template
# from decimal import Decimal
from currency.models import Currency, CurrencyRate

register = template.Library()


def get_currency(currency_code: str):
    return Currency.objects.get(code=currency_code)


def get_currency_rate(currency_from, currency_to):
    return CurrencyRate.objects.get(currency_from=currency_from, currency_to=currency_to).rate


@register.simple_tag
def product_tags(product_fields, field_name_code):
    try:
        return product_fields[field_name_code]
    except (KeyError, TypeError):
        return ""


@register.simple_tag
def show_price(product, price_type: str, need_currency_code: str, currency_cache, currency_rate_cache):
    currency_field = "{}_currency".format(price_type)
    currency = getattr(product, currency_field)
    currency_symbol_display = currency.symbol
    currency_code = currency.code
    rate = 1
    if price_type in ('wholesale', 'retail'):
        reverse_rate = False
        # TODO: Need fix (maybe use cache for currencies and rates?)
        if need_currency_code is not None and len(need_currency_code) and need_currency_code != currency_code:
            try:
                if need_currency_code in currency_cache:
                    currency_to = currency_cache[need_currency_code]
                else:
                    currency_to = get_currency(need_currency_code)
                    currency_cache[need_currency_code] = currency_to
                # rate = CurrencyRate.objects.get(currency_from=currency, currency_to=currency_to).rate

                rate_cache_key = "{}_{}".format(currency.code, currency_to.code)
                if rate_cache_key in currency_rate_cache:
                    rate = currency_rate_cache[rate_cache_key]
                else:
                    rate = get_currency_rate(currency, currency_to)
                    currency_rate_cache[rate_cache_key] = rate

                currency_symbol_display = currency_to.symbol
            except (Currency.DoesNotExist, CurrencyRate.DoesNotExist):  # Need remove except
                try:
                    if need_currency_code in currency_cache:  # FIXME: See above code
                        currency_to = currency_cache[need_currency_code]
                    else:
                        currency_to = get_currency(need_currency_code)
                        currency_cache[need_currency_code] = currency_to

                    rate_cache_key = "{}_{}".format(currency_to.code, currency.code)
                    if rate_cache_key in currency_rate_cache:
                        rate = currency_rate_cache[rate_cache_key]
                    else:
                        rate = get_currency_rate(currency_to, currency)
                        currency_rate_cache[rate_cache_key] = rate
                    reverse_rate = True
                    currency_symbol_display = currency_to.symbol
                except (Currency.DoesNotExist, CurrencyRate.DoesNotExist):
                    raise

        price_field = "{}_price".format(price_type)
        price = getattr(product, price_field)
        if not reverse_rate:
            price_converted = "{:.2f}".format(price * rate)
        else:
            price_converted = "{:.2f}".format(price / rate)
        return "{}{}".format(price_converted, currency_symbol_display)
    else:
        raise RuntimeError("Unknown price type: {}".format(price_type))


@register.simple_tag
def category_level(level: int):
    return "---" * level
