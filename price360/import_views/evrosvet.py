import uuid
import os
import re
from django.shortcuts import render, redirect
from django import forms
from django.conf import settings
import pyexcel as pe

from price360.models import ImportResult, ImportCategory, ImportProduct, ImportManufacturer
from currency.models import Currency


class ImportStep1Form(forms.Form):
    file = forms.FileField(label="Excel файл")


def evrosvet_import_step_1(request):
    if request.method.lower() == 'post':
        form = ImportStep1Form(request.POST, request.FILES)
        if form.is_valid():
            filename = str(uuid.uuid4())
            path = "{}/evrosvet/{}.xls".format(settings.IMPORT_DIR, filename)
            with open(path, "w+b") as f:
                f.write(form.cleaned_data['file'].read())
                request.session['import_filename'] = "{}.xls".format(filename)
                return redirect('/products/import_evrosvet/step/2/')
    else:
        form = ImportStep1Form()

    context = {'form': form}

    return render(request, 'price360/import_evrosvet/step1.html', context=context)


def evrosvet_import_step_2(request):
    stock_regexp = re.compile("\d+")

    def parse_stock_raw(stock_raw: str):
        num = stock_regexp.findall(stock_raw)
        if len(num) == 2:  # Example: [10, 20]
            return [int(num[0]), int(num[1])]
        elif len(num) == 1:  # Example: [10]
            return [1, int(num[0])]
        else:
            return None

    filename = request.session['import_filename']
    full_path = os.path.join(settings.IMPORT_DIR, 'evrosvet', filename)

    # print(full_path)
    code_col = 0
    name_col = 1
    wholesale_price_col = 3
    retail_price_col = 5
    stock_col = 7

    start_row = 8

    book = pe.get_book(file_name=full_path)
    sheet = book[0]

    cur_row = 0

    wholesale_currency = Currency.objects.get(code='UAH')
    retail_currency = Currency.objects.get(code='UAH')
    manufacturer, created = ImportManufacturer.objects.get_or_create(defaults={'name': 'Евросвет'})

    rows = 0
    success_rows = 0

    import_result = ImportResult(filename=filename, rows=0, success_rows=0, user=request.user,
                                 shop=request.user.last_shop, import_price=ImportResult.IMPORT_EVROSVET)
    import_result.save()
    category_obj = None

    for row in sheet:
        stock = ImportProduct.STOCK_YES
        stock_num = [0, 0]

        cur_row += 1
        if cur_row < start_row:
            continue

        code = row[code_col]
        name = row[name_col].replace('\n', ' ').strip()
        name = re.sub(' +', ' ', name)
        wholesale_price = row[wholesale_price_col]
        retail_price = row[retail_price_col]
        stock_raw = row[stock_col]

        if not code and name and not retail_price:  # It's category
            try:
                category_obj = ImportCategory.objects.get(name=name, shop=request.user.last_shop,
                                                          import_result=import_result)
            except ImportCategory.DoesNotExist:
                category_obj = ImportCategory(name=name, shop=request.user.last_shop, import_result=import_result)
                category_obj.save()

        if code and name and retail_price:
            rows += 1
            if category_obj:
                import_product = ImportProduct()
                import_product.code = code
                import_product.name = name
                import_product.wholesale_price = wholesale_price
                import_product.wholesale_currency = wholesale_currency
                import_product.retail_price = retail_price
                import_product.retail_currency = retail_currency
                import_product.category = category_obj
                import_product.manufacturer = manufacturer
                import_product.stock_raw = stock_raw

                print(stock_raw)
                if 'Нет' in stock_raw:  # TODO: Need many different - "Нет", "Да", "Есть", "Остаток".
                    stock = ImportProduct.STOCK_NO

                if stock == ImportProduct.STOCK_YES:  # TODO: Need parse range in 'stock_raw' field
                    stock_num_parsed = parse_stock_raw(stock_raw)
                    if stock_num_parsed is not None:
                        stock_num = stock_num_parsed
                    else:
                        stock_num = [1, 10]

                import_product.stock = stock
                import_product.stock_num = stock_num
                import_product.guarantee = 0
                import_product.fields = {}
                import_product.import_result = import_result

                print(import_product)
                import_product.save()

                success_rows += 1

                # print(name)
