from price360.models import Shop


def shops(request):
    if request.user.is_authenticated:
        return {'SHOPS': Shop.objects.filter(is_active=True).all(), 'CUR_SHOP_ID': request.user.last_shop_id}
    else:
        return {}
