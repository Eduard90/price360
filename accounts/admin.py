# from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
#
# from .models import User
# from .forms import UserChangeForm, UserCreationForm
# from django.utils.translation import ugettext, ugettext_lazy as _
#
#
# class UserAdmin(UserAdmin):
#     form = UserChangeForm
#     add_form = UserCreationForm
#
#     fieldsets = (
#         (None, {'fields': ('username', 'password', 'domain')}),
#         (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'phone')}),
#         (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
#                                        'groups', 'user_permissions')}),
#         (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
#     )
#
#     list_display = ('username', 'email', 'phone', 'first_name', 'last_name', 'is_staff')
#     list_filter = ('is_active', 'is_staff', 'is_superuser')
#
#
# admin.site.register(User, UserAdmin)
