# from django.core.mail import send_mail
# from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import AbstractUser
# from django.utils import timezone
# from django.utils.http import urlquote


class User(AbstractUser):
    last_shop = models.ForeignKey('price360.Shop', verbose_name='Магазин (последний)', null=True)
#     phone = models.CharField("Телефон", max_length=12, blank=False)
#     USERNAME_FIELD = 'phone'
#     REQUIRED_FIELDS = ['phone']
