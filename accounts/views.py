from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('login', '')
        password = request.POST.get('password', '')
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('/')
        else:
            messages.warning(request, 'Ошибка. Неправильный логин или пароль.')

    return render(request, 'accounts/login.html')


def logout_view(request):
    logout(request)
    return redirect('/login/')
