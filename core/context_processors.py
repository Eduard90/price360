from price360.models import Category


def menu_categories(request):
    if request.user.is_authenticated:
        return {'categories': Category.objects.filter(shop=request.user.last_shop).all()}
    else:
        return {'categories': []}
