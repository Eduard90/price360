from django.contrib import admin

from currency.models import Currency, CurrencyRate


# Register your models here.

@admin.register(Currency)
class CurrencyModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name')
    list_display_links = ('id', 'code')


@admin.register(CurrencyRate)
class CurrencyRateModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'currency_from', 'currency_to', 'rate', 'modified')
    list_display_links = ('id', 'currency_from', 'currency_to')
