from django.db import models
from model_utils.models import TimeStampedModel
from dirtyfields import DirtyFieldsMixin


# Create your models here.


class Currency(DirtyFieldsMixin, TimeStampedModel, models.Model):
    code = models.CharField("Код", max_length=3, blank=False, unique=True, db_index=True)
    name = models.CharField("Название", max_length=100, blank=False)
    symbol = models.CharField("Символ", max_length=4, blank=False, db_index=True)

    class Meta:
        ordering = ['name']
        verbose_name = "Валюта"
        verbose_name_plural = "Валюты"

    def __str__(self):
        return self.code


class CurrencyRate(DirtyFieldsMixin, TimeStampedModel, models.Model):
    currency_from = models.ForeignKey(Currency, verbose_name="Из", related_name='rate_from')
    currency_to = models.ForeignKey(Currency, verbose_name="В", related_name='rate_to')
    rate = models.DecimalField("Курс", max_digits=6, decimal_places=3)

    class Meta:
        verbose_name = "Курсы"
        verbose_name_plural = "Курсы"

    def __str__(self):
        return "1{} -> {}{}".format(self.currency_from, self.rate, self.currency_to)
