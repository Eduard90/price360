from django.core.management.base import BaseCommand, CommandError
import requests

from currency.models import Currency, CurrencyRate


class UpdateRateException(Exception):
    pass


class UnknownCurrencyCodeTo(UpdateRateException):
    def __init__(self, currency_code_to: str):
        super().__init__("Unknown currency_code_to: {}".format(currency_code_to))


class CurrencyResponseError(UpdateRateException):
    def __init__(self, currency_code_from: str):
        super().__init__("Can't find '{}' in response.".format(currency_code_from))


class Command(BaseCommand):
    help = 'Run update rate'

    def _usd_to_rur(self):
        self.__get_rate_for_currency('USD', 'RUR')

    def _uah_to_rur(self):
        self.__get_rate_for_currency('UAH', 'RUR')

    def _usd_to_uah(self):
        self.__get_rate_for_currency('USD', 'UAH')

    def _rur_to_uah(self):
        self.__get_rate_for_currency('RUR', 'UAH')

    def __get_rate_for_currency(self, currency_code_from: str, currency_code_to: str):
        if currency_code_to == 'RUR':
            resp = requests.get("https://www.cbr-xml-daily.ru/daily_json.js")
            resp_json = resp.json()
            try:
                valute = resp_json['Valute'][currency_code_from]
            except KeyError:
                raise CurrencyResponseError(currency_code_from)
            rate = valute['Value'] / valute['Nominal']
        elif currency_code_to == 'UAH':
            rate = None
            resp = requests.get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
            resp_json = resp.json()
            currency_code_from_ = currency_code_from
            if currency_code_from == 'RUR':
                currency_code_from_ = 'RUB'  # Hack for bank.gov.ua

            for currency_dict in resp_json:
                if currency_dict['cc'] == currency_code_from_:
                    rate = currency_dict['rate']
            if rate is None:
                raise CurrencyResponseError(currency_code_from)
        else:
            raise UnknownCurrencyCodeTo(currency_code_to)

        currency_from = Currency.objects.get(code=currency_code_from)
        currency_to = Currency.objects.get(code=currency_code_to)
        currency_rate, created = CurrencyRate.objects.get_or_create(currency_from=currency_from,
                                                                    currency_to=currency_to,
                                                                    defaults={'rate': rate})
        if not created:
            currency_rate.rate = rate
            currency_rate.save()

    def add_arguments(self, parser):
        parser.add_argument('currency_from', nargs=1, default=None, type=str)
        parser.add_argument('currency_to', nargs=1, default=None, type=str)

    def handle(self, *args, **options):
        currency_from_code = options['currency_from'][0]
        currency_to_code = options['currency_to'][0]
        currency_from = Currency.objects.get(code=currency_from_code)
        currency_to = Currency.objects.get(code=currency_to_code)

        convert_method_name = '_{}_to_{}'.format(currency_from.code.lower(), currency_to.code.lower())
        convert_method = getattr(self, convert_method_name, None)
        if convert_method is None:
            raise CommandError("Can't find convert method '{}'".format(convert_method_name))

        try:
            convert_method()
        except UpdateRateException as e:
            self.stdout.write("Error: {}".format(self.style.ERROR(e)))
        else:
            self.stdout.write(self.style.SUCCESS("Rate '{} -> {}' updated".format(currency_from_code, currency_to_code)))
